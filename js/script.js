
/*FIXED NAVBAR
$(document).ready(function() {
	$(window).scroll(function() {
	  if ($(this).scrollTop() > 100){

				 $('.navbar').addClass('fixed');
         $(".navbar").css("background-color","rgba(0, 0, 0, 0.5)");
         $(".hamburger").css("margin-top", "5px");
         $(".social_nav").css("margin-top", "5px");
         $(".my-logo").css("width", "50px");
			 }

			 else {
				 $('.navbar').removeClass('fixed');
         $(".navbar").css("background-color","transparent");
         $(".hamburger").css("margin-top", "30px");
         $(".social_nav").css("margin-top", "20px");
         $(".my-logo").css("width", "100px");
			 }
		});
});*/



/*ClONA - HOVER - PORTFÓLIO */

$(document).ready(function () {

  $(".prelink").hide();

   $(".projects").mouseover(function() {
      $(this).addClass("clona");
      $(this).find(".prelink").show();
    });

   $(".projects").mouseout(function() {
      $(this).removeClass("clona");
      $(this).find(".prelink").hide();
    });
});







$(document).ready(function() {

	$(".back").click(function () {
		$("html", "body").animate( { scrollTop: 0 }, 1000);
	});

});

$(document).ready(function() {

  //window and animation items
  var animation_elements = $.find('.animation-element');
  var web_window = $(window);

  //check to see if any animation containers are currently in view
  function check_if_in_view() {
    //get current window information
    var window_height = web_window.height();
    var window_top_position = web_window.scrollTop();
    var window_bottom_position = (window_top_position + window_height);

    //iterate through elements to see if its in view
    $.each(animation_elements, function() {

      //get the element sinformation
      var element = $(this);
      var element_height = $(element).outerHeight();
      var element_top_position = $(element).offset().top;
      var element_bottom_position = (element_top_position + element_height);

      //check to see if this current container is visible (its viewable if it exists between the viewable space of the viewport)
      if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
        element.addClass('in-view');
      } else {
        element.removeClass('in-view');
      }
    });

  }

  //on or scroll, detect elements in view
  $(window).on('scroll resize', function() {
      check_if_in_view()
    })
    //trigger our scroll event on initial load
  $(window).trigger('scroll');

});


///TYPING

/*function autoType(elementClass, typingSpeed){
  var thhis = $(elementClass);
  thhis.css({
    "position": "relative",
    "display": "inline-block"
  });
  thhis.prepend('<div class="cursor" style="right: initial; left:0;"></div>');
  thhis = thhis.find(".text-js");
  var text = thhis.text().trim().split('');
  var amntOfChars = text.length;
  var newString = "";
  thhis.text("|");
  setTimeout(function(){
    thhis.css("opacity",1);
    thhis.prev().removeAttr("style");
    thhis.text("");
    for(var i = 0; i < amntOfChars; i++){
      (function(i,char){
        setTimeout(function() {
          newString += char;
          thhis.text(newString);
        },i*typingSpeed);
      })(i+1,text[i]);
    }
  },1500);
}

$(document).ready(function(){
  // Now to start autoTyping just call the autoType function with the
  // class of outer div
  // The second paramter is the speed between each letter is typed.
  autoType(".type-js",200);


});*/


/// SCROLL DOWN ON CLICK

  $(".scroll-down").click(function() {
    $("html, body").animate ({
      scrollTop: $("#about").offset().top},"slow");
    });


/// PUSH MENU

  $(document).ready(function() {
    $menuLeft = $('.pushmenu-left');
    $nav_list = $('.hamburger');

    $nav_list.click(function() {
      $(this).toggleClass('active');
      $('.pushmenu-push').toggleClass('pushmenu-push-toright');
      $menuLeft.toggleClass('pushmenu-open');
    });
  });

    $(document).ready(function() {
      $(".hide-li").click(function () {
        $menuLeft.toggleClass('pushmenu-open');
        $hamburger.toggleClass("is-active");
      })
});

  /// HAMBURGER


  var $hamburger = $(".hamburger");
  $hamburger.on("click", function(e) {
    $hamburger.toggleClass("is-active");
    // Do something else, like open/close menu
  });


  /// PARALLAX

  function simpleParallax() {
    //This variable is storing the distance scrolled
    var scrolled = $(window).scrollTop() + 1;

    //Every element with the class "scroll" will have parallax background
    //Change the "0.3" for adjusting scroll speed.
    $('.scroll').css('background-position', '0' + -(scrolled * 0.3) + 'px');
}
//Everytime we scroll, it will fire the function
$(window).scroll(function (e) {
    simpleParallax();
});


//BACK TO TOP
var backToTop = $(".back-to-top");

    backToTop
        .hide()
        .appendTo("body")
        .on("click", function() {
         $("body").animate({ scrollTop: 0 }, 1000);
    });


var win = $(window);
    win.on("scroll", function() {
        if (win.scrollTop() > 500 ) backToTop.fadeIn();
        else backToTop.hide();
    });
